package com.amazon.test.rabbit;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MessageListener {

    Button btnSendSMS;
    EditText editMobile;
    private static final int SEND_PERMISSION_REQUEST_CODE = 1;
    private static final int STATE_PERMISSION_REQUEST_CODE = 2;
    private static final int READ_PERMISSION_REQUEST_CODE = 10;
    private static final int RECV_PERMISSION_REQUEST_CODE = 20;
    private List<String> trackingIDs = new ArrayList<>();

    BroadcastReceiver broadcastReceiver =  new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            String message = b.getString("message");

            Log.e("newmesage", "" + message);
            Toast.makeText(context, "Don't deliver " + message, Toast.LENGTH_LONG).show();
            Button btnSendSMSa;
            btnSendSMSa = (Button) findViewById(R.id.btnDeliver0);
            TextView textViewa;
            textViewa = (TextView) findViewById(R.id.textView0);
            if(textViewa.getText().toString().equals(message)) {
                btnSendSMSa.setVisibility(View.INVISIBLE);
            }

            Button btnSendSMSb;
            btnSendSMSb = (Button) findViewById(R.id.btnDeliver1);
            textViewa = (TextView) findViewById(R.id.textView1);
            if(textViewa.getText().toString().equals(message)) {
                btnSendSMSb.setVisibility(View.INVISIBLE);
            }

            Button btnSendSMSc;
            btnSendSMSc = (Button) findViewById(R.id.btnDeliver2);
            textViewa = (TextView) findViewById(R.id.textView2);
            if(textViewa.getText().toString().equals(message)) {
                btnSendSMSc.setVisibility(View.INVISIBLE);
            }
        }
    };


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/**
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            String[] permissions = {Manifest.permission.SEND_SMS};

            requestPermissions(permissions, SEND_PERMISSION_REQUEST_CODE);

            String[] permissions1 = {Manifest.permission.READ_PHONE_STATE};

            requestPermissions(permissions1, STATE_PERMISSION_REQUEST_CODE);

            String[] permissions2 = {Manifest.permission.READ_SMS};
            requestPermissions(permissions2, READ_PERMISSION_REQUEST_CODE);

            String[] permissions3 = {Manifest.permission.RECEIVE_SMS};
            requestPermissions(permissions3, RECV_PERMISSION_REQUEST_CODE);

        }
**/

        List<String> permissions = new ArrayList<>();

        int hasSMSPermission = checkSelfPermission( Manifest.permission.SEND_SMS );

        if( hasSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.SEND_SMS );
        }

        int hasRecvPermission = checkSelfPermission( Manifest.permission.RECEIVE_SMS );

        if( hasRecvPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.RECEIVE_SMS );
        }

        int hasStatePermission = checkSelfPermission( Manifest.permission.READ_PHONE_STATE );

        if( hasStatePermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.READ_PHONE_STATE );
        }

        int hasReadSMSPermission = checkSelfPermission( Manifest.permission.READ_SMS );

        if( hasReadSMSPermission != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.READ_SMS );
        }

        if( !permissions.isEmpty() ) {
            requestPermissions( permissions.toArray( new String[permissions.size()] ), RECV_PERMISSION_REQUEST_CODE );
        }


        btnSendSMS = (Button) findViewById(R.id.btnDeliver0);
        btnSendSMS.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                sendSMS("DELIVERED TBA12345");
            }
        });

        Button btnSendSMS1;
        btnSendSMS1 = (Button) findViewById(R.id.btnDeliver1);
        btnSendSMS1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                sendSMS("DELIVERED TBA00000");
            }
        });

        Button btnSendSMS2;
        btnSendSMS2 = (Button) findViewById(R.id.btnDeliver2);
        btnSendSMS2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                sendSMS("DELIVERED TBA99999");
            }
        });

        registerReceiver(broadcastReceiver, new IntentFilter("broadCastName"));

    }

    @Override
    public void messageReceived(String message) {
        Toast.makeText(this, "New Message Received: " + message, Toast.LENGTH_SHORT).show();
    }

    public void receiveSMS() {
        StringBuilder smsBuilder = new StringBuilder();
        final String SMS_URI_INBOX = "content://sms/inbox";
        final String SMS_URI_ALL = "content://sms/";
        try {
            Uri uri = Uri.parse(SMS_URI_INBOX);
            String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
            Cursor cur = getContentResolver().query(uri, projection, "address='+918076939622'", null, "date desc");
            if (cur.moveToFirst()) {
                int index_Address = cur.getColumnIndex("address");
                int index_Person = cur.getColumnIndex("person");
                int index_Body = cur.getColumnIndex("body");
                int index_Date = cur.getColumnIndex("date");
                int index_Type = cur.getColumnIndex("type");
                do {
                    String strAddress = cur.getString(index_Address);
                    int intPerson = cur.getInt(index_Person);
                    String strbody = cur.getString(index_Body);
                    long longDate = cur.getLong(index_Date);
                    int int_Type = cur.getInt(index_Type);

                    smsBuilder.append("[ ");
                    smsBuilder.append(strAddress + ", ");
                    smsBuilder.append(intPerson + ", ");
                    smsBuilder.append(strbody + ", ");
                    smsBuilder.append(longDate + ", ");
                    smsBuilder.append(int_Type);
                    smsBuilder.append(" ]\n\n");
                } while (cur.moveToNext());
                btnSendSMS = (Button) findViewById(R.id.btnDeliver0);
                btnSendSMS.setVisibility(View.INVISIBLE);

                if (!cur.isClosed()) {
                    cur.close();
                    cur = null;
                }
            } else {
                smsBuilder.append("no result!");
            } // end if
        } catch (SQLiteException ex) {
            Log.d("SQLiteException", ex.getMessage());
        }
    }

    public void sendSMS(String message)
    {
        SmsManager sm = SmsManager.getDefault();
        editMobile = (EditText) findViewById(R.id.mobile);
        String number = "1";
        if(editMobile.getText().toString().isEmpty()) {
            number = "+919411631043";
        }
        else
        {
            number = editMobile.getText().toString();
        }
        sm.sendTextMessage(number, null, message, null, null);
    }
}
